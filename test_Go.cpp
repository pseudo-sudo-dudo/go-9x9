// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <string>    // string

#include "gtest/gtest.h"

#include "GM.hpp"
#include "Go.hpp"
#include "GoStone.hpp"

using namespace std;

string const emptyBoard = "   A B C D E F G H I\n9  . . . . . . . . . \n8  . . . . . . . . . \n7  . . . . . . . . . \n6  . . . . . . . . . \n5  . . . . . . . . . \n4  . . . . . . . . . \n3  . . . . . . . . . \n2  . . . . . . . . . \n1  . . . . . . . . . \n\n";

TEST(GoBoard, test1)
{
    Go goGame = Go();

    string result = goGame.printBoard();
    string output = emptyBoard;
    ASSERT_EQ(output, result);
};

TEST(GoHeader, test2)
{
    GM gm = GM();

    string result = gm.header();
    string output = "Turn: 1   Black Captures: 0   White Captures: 0\nBlack's Turn\n";
    ASSERT_EQ(output, result);
}

TEST(GoStonePlace, test3)
{
    GM gm = GM();

    string place = "A4";
    gm.placeStone(place);
    string result = gm.printBoard();
    string output = "   A B C D E F G H I\n9  . . . . . . . . . \n8  . . . . . . . . . \n7  . . . . . . . . . \n6  . . . . . . . . . \n5  . . . . . . . . . \n4  O . . . . . . . . \n3  . . . . . . . . . \n2  . . . . . . . . . \n1  . . . . . . . . . \n\n";
    ASSERT_EQ(output, result);
}

TEST(GoStoneTurn, test3)
{
    GM gm = GM();

    string place = "A4";
    gm.placeStone(place);
    gm.switchColor();
    place = "B4";
    gm.placeStone(place);
    string result = gm.printBoard();
    string output = "   A B C D E F G H I\n9  . . . . . . . . . \n8  . . . . . . . . . \n7  . . . . . . . . . \n6  . . . . . . . . . \n5  . . . . . . . . . \n4  O @ . . . . . . . \n3  . . . . . . . . . \n2  . . . . . . . . . \n1  . . . . . . . . . \n\n";
    ASSERT_EQ(output, result);
}