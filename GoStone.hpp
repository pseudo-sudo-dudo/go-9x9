// --------
// GoStone.hpp
// --------

#ifndef GoStone_hpp
#define GoStone_hpp

// --------
// includes
// --------

using namespace std;

// ----
// GoStone
// ----

enum Stone
{
    EMPTY,
    BLACK,
    WHITE
};

class GoStone
{
private:
    int _liberties;
    int _state;

public:
    // Constructor
    /*
    Creates the GoStone.
    given an alive or dead state
    will be given an age (or default to 0)
    */
    // Constructor
    GoStone()
    {
        _state = EMPTY;
    }

    // void neighborhood(GoStone *up, GoStone *right, GoStone *down, GoStone *left)
    // {
    //     // if (_state)
    //     // {
    //     //     up->cardinal_neighbor();
    //     //     right->cardinal_neighbor();
    //     //     down->cardinal_neighbor();
    //     //     left->cardinal_neighbor();
    //     // }
    // }

    // int cardinal_neighbor()
    // {
    //     // ++_conNeighbors;
    //     // ++_fredNeighbors;

    //     // // returns count for tests/checking
    //     // return _conNeighbors;
    // }

    void place(string &color)
    {
        if (color == "Black")
        {
            _state = BLACK;
        }
        else
        {
            _state = WHITE;
        }
    }

    void updateLibs(int &libCount)
    {
        _liberties = libCount;
    }

    string print()
    {
        string cellPrint;
        switch (_state)
        {
        case EMPTY:
            cellPrint += ".";
            break;
        case BLACK:
            cellPrint += "O";
            break;
        case WHITE:
            cellPrint += "@";
            break;
        default: // A GRAVE ERROR
            cellPrint += "ERROR";
        }
        return cellPrint;
    }
};

#endif // GoStone_hpp