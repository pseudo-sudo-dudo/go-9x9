.DEFAULT_GOAL := all
SHELL         := bash

ifeq ($(shell uname -s), Go)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := clang++
    CXXFLAGS      := --coverage -g -std=c++20 -I$(INCLUDE_PATH) -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := llvm-cov gcov
    GTEST         := /usr/local/include/gtest
    LDFLAGS       := -lgtest -lgtest_main
    LIB           := $(LIBRARY_PATH)
    VALGRIND      :=
else ifeq ($(shell uname -p), x86_64)
    ASTYLE        := astyle
    BOOST         := /lusr/opt/boost-1.82/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-11
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov-11
    GTEST         := /usr/include/gtest
    LDFLAGS       := -L/usr/local/opt/boost-1.77/lib/ -lgtest -lgtest_main -pthread
    LIB           := /usr/lib/x86_64-linux-gnu
    VALGRIND      := valgrind-3.17
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov
    GTEST         := /usr/include/gtest
    LDFLAGS       := -lgtest -lgtest_main -pthread
    LIB           := /usr/lib
    VALGRIND      := valgrind
endif

# run docker
docker:
	docker run --rm -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Go.log.txt:
	git log > Go.log.txt

# get git status
status:
	make --no-print-directory clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Go code repo
pull:
	make --no-print-directory clean
	@echo
	git pull
	git status

# upload files to the Go code repo
push:
	make --no-print-directory clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Go.hpp
	-git add Go.log.txt
	-git add html
	git add Makefile
	git add README.md
	git add run_GoConway.cpp
	-git add run_Go.ctd.txt
	git add test_Go.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
run_Go: Go.hpp run_Go.cpp
	-$(CPPCHECK) run_Go.cpp
	$(CXX) $(CXXFLAGS) run_Go.cpp -o run_Go

# compile test harness
test_Go: Go.hpp test_Go.cpp
	-$(CPPCHECK) test_Go.cpp
	$(CXX) $(CXXFLAGS) test_Go.cpp -o test_Go $(LDFLAGS)

# run/test files, compile with make all
FILES :=           \
    run_Go  \
    test_Go

# compile all
all: $(FILES)

# execute test harness with coverage
test: test_Go
	$(VALGRIND) ./test_Go
ifeq ($(shell uname -s), Darwin)
	$(GCOV) test_Go-test_Go.cpp | grep -B 2 "cpp.gcov"
else
	$(GCOV) test_Go.cpp | grep -B 2 "cpp.gcov"
endif

# clone the Go test repo
../cs371p-Go-tests:
	git clone https://gitlab.com/pseudo-sudo-dudo/cs371p-Go-tests.git ../cs371p-Go-tests

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g run_Go.ctd.txt | grep -vP '^!' > run_Go.gen.txt
	$(CHECKTESTDATA) run_Go.ctd.txt run_Go.gen.txt

# execute the run harness against your test files in the Go test repo and diff with the expected output
# change pseudo-sudo-dudo to your GitLab-ID
run: run_Go ../cs371p-Go-tests
# uncomment line below when run_Go.ctd.txt is available
	$(CHECKTESTDATA) run_Go.ctd.txt ../cs371p-Go-tests/pseudo-sudo-dudo-run_GoConway.in.txt
	./run_Go < ../cs371p-Go-tests/pseudo-sudo-dudo-run_GoConway.in.txt > run_Go.tmp.txt
	diff run_Go.tmp.txt ../cs371p-Go-tests/pseudo-sudo-dudo-run_GoConway.out.txt

# execute the run harness against all of the test files in the Go test repo and diff with the expected output
run-all: run_Go ../cs371p-Go-tests
	-@for v in `ls ../cs371p-Go-tests/*.in.txt`;         \
    do                                                          \
        echo $(CHECKTESTDATA) run_Go.ctd.txt $${v};      \
             $(CHECKTESTDATA) run_Go.ctd.txt $${v};      \
        echo ./run_Go \< $${v} \> run_Go.tmp.txt; \
             ./run_Go  < $${v}  > run_Go.tmp.txt; \
        echo diff run_Go.tmp.txt $${v/.in/.out};         \
             diff run_Go.tmp.txt $${v/.in/.out};         \
    done

# auto format the code
format:
	$(ASTYLE) Go.hpp
	$(ASTYLE) run_Go.cpp
	$(ASTYLE) test_Go.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check the existence of check files
check: .gitignore .gitlab-ci.yml Go.log.txt html

# remove executables and temporary files
clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -f  $(FILES)
	rm -rf *.dSYM

# remove executables, temporary files, and generated files
scrub:
	make --no-print-directory clean
	rm -f  Go.log.txt
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(ASTYLE)
	@echo
	$(ASTYLE) --version

	@echo
	which $(CHECKTESTDATA)
	@echo
	$(CHECKTESTDATA) --version | head -n 1

	@echo
	which cmake
	@echo
	cmake --version | head -n 1

	@echo
	which $(CPPCHECK)
	@echo
	$(CPPCHECK) --version

	@echo
	which $(DOXYGEN)
	@echo
	$(DOXYGEN) --version

	@echo
	which $(CXX)
	@echo
	$(CXX) --version | head -n 1

	@echo
	which $(GCOV)
	@echo
	$(GCOV) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

ifneq ($(VALGRIND),)
	@echo
	which $(VALGRIND)
	@echo
	$(VALGRIND) --version
endif

	@echo
	which vim
	@echo
	vim --version | head -n 1

	@echo
	grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	ls -al $(GTEST)/gtest.h
	@echo
	pkg-config --modversion gtest
	@echo
	ls -al $(LIB)/libgtest*.a
