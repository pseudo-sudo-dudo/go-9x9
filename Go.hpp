// --------
// Go.hpp
// --------

#ifndef Go_hpp
#define Go_hpp

// --------
// includes
// --------

#include <tuple>  // tuple
#include <vector> // vector
#include <string>
#include <iostream> // cin, cout, endl
#include <typeindex>
#include "GoStone.hpp"

using namespace std;

// ----
// Go
// ----

class Go
{
private:
    vector<vector<GoStone>> _board;
    int _rows;
    int _cols;
    bool _lastPrint;

public:
    static const int BOARD_SIZE = 9;

    Go()
    {
        _rows = BOARD_SIZE;
        _cols = BOARD_SIZE;
        _lastPrint = false;
        _board.resize(BOARD_SIZE + 2, vector<GoStone>(BOARD_SIZE + 2));
    }

    // Updates placed stone on the board
    void placeStone(string &translated, string &color)
    {
        char colChar = translated.at(0);
        int row = translated.at(1) - '0' + 1;
        int col = (colChar - 'A') + 1;

        _board[row][col].place(color);
    }

    // Counts the number of liberties
    int libertyCount(int &row, int &col)
    {
        _board[row][col];
        return 0;
    }

    void lastTime()
    {
        _lastPrint = true;
    }

    string printBoard()
    {
        stringstream result;

        // Putting the labels on the columns
        result << "   A B C D E F G H I\n";

        for (int r = _rows + 1; r > 1; r--)
        {
            result << r - 1 << "  ";
            for (int c = 1; c < _cols + 1; c++)
            {
                string cellPrint = _board[r][c].print();
                result << cellPrint << " ";
            }
            result << "\n";
        }

        return result.str() + "\n";
    }
}

;

#endif // Go_hpp