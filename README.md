# Go 9x9

## Introduction

My name is Sean, a computer scientist who graduated from UT. I am working on this as a personal project that I hope to get completed by the end of this year! I'm starting on a 9x9 board and hopefully will upgrade to bigger boards after this works.

## What is Go?

Go is an ancient strategic board game that originated in China over 2,500 years ago and is still widely played today. Played on a gridded board with black and white stones, the objective is to control territory by surrounding empty intersections and capturing opponents' stones. Players take turns placing stones on the intersections, aiming to create strong groups and influence across the board. Go is known for its profound depth and complexity, requiring strategic foresight, spatial awareness, and the ability to balance offense and defense. Despite its simple rules, the game offers a vast strategic landscape, and mastery requires years of practice, making it a timeless and revered classic in the world of board gaming.

## How to play:

Each player will take turns in the terminal inputing where they wish to place their stones. The board will be divided into rows (numbers) and columns (letters). You will put in your input by inputing COLUMN NUMBER (i.e. C3). It will continue to play until each player inputs "PASS" or "P" successively and then the game will be completed.
