// --------------
// run_LifeConway.cpp
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl
#include <cassert>  // asserts
#include <sstream>
#include <string>
#include <tuple>

#include "Go.hpp"
#include "GM.hpp"
#include "GoStone.hpp"

// -----------
// populate
// -----------

// void populate(Life &life, int numAlive)
// {
//     for (int i = 0; i < numAlive; ++i)
//     {
//         int x, y = 0;
//         std::cin >> x >> y;
//         life.addLife(x, y);
//     }
// }

// ----
// main
// ----

int main()
{

    GM gaming = GM();
    gaming.startGame();
    gaming.printTurn();

    while (!gaming._gameOver)
    {
        gaming.printTurn();
    }
}