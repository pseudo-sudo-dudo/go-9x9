// --------
// GM.hpp
// --------

#ifndef GM_hpp
#define GM_hpp

// --------
// includes
// --------

#include <tuple>  // tuple
#include <vector> // vector
#include <string>
#include <iostream> // cin, cout, endl
#include <typeindex>
#include "GoStone.hpp"
#include "Go.hpp"

using namespace std;

// ----
// GM
// ----

// This is the class that will allow run the Go board by updating the board and
// changing keeping track of the points. Will change display each round

class GM
{
private:
    Go _goBoard;
    int _komi;
    int _blackScore;
    int _whiteScore;
    int _blackCaptures;
    int _whiteCaptures;
    int _turn;
    string _colorTurn;

public:
    bool _gameOver;

public:
    static const int BOARD_SIZE = 9;

    GM()
    {
        _goBoard = Go();
        _komi = 5.5;
        _blackScore = 0;
        _whiteScore = 0;
        _blackCaptures = 0;
        _whiteCaptures = 0;
        _turn = 1;
        _colorTurn = "Black";
        _gameOver = false;
    }

    string header()
    {
        stringstream ss;

        ss << "Turn: " << _turn << "   Black Captures: " << _blackCaptures << "   White Captures: " << _whiteCaptures << endl;
        ss << _colorTurn << "'s Turn" << endl;

        return ss.str();
    }

    void startGame()
    {
        cout << "Welcome to Go!" << endl;
        cout << "Go is a strategic board game for two players that originated in ancient China. The objective is to control a larger area of the board by placing stones on the intersections, capturing opponent stones, and creating territories." << endl;
        cout << "To input your move, please do LETTER NUMBER together with no space. an Example: A2 or H8.\n"
             << endl;
    }

    // Asks what the user wants to do on their turn
    string askTurn()
    {
        string input;

        cout << _colorTurn << "'s Move [LetterNumber]: ";
        cin >> input;
        return input;
    }

    string translateInput(string &input)
    {
        // Check if the length of the input is exactly 2
        if (input.length() != 2)
        {
            cout << "Invalid input length.\n";
            return "INCORRECT";
        }

        // Check if the first character is a letter
        char firstChar = input[0];
        if (!std::isalpha(firstChar))
        {
            cout << "First character is not a letter.\n";
            return "INCORRECT";
        }

        // Convert the first character to uppercase
        firstChar = std::toupper(firstChar);

        // Check if the first character is between 'A' and 'I'
        if (firstChar < 'A' || firstChar > 'I')
        {
            cout << "First character is not between A and I.\n";
            return "INCORRECT";
        }

        // Check if the second character is a digit
        char secondChar = input[1];
        if (!std::isdigit(secondChar))
        {
            cout << "Second character is not a digit.\n";
            return "INCORRECT";
        }

        // Check if the second character is between '1' and '9'
        if (secondChar < '1' || secondChar > '9')
        {
            cout << "Second character is not between 1 and 9.\n";
            return "INCORRECT";
        }

        // If all checks pass, standardize the input
        input[0] = firstChar;

        // Return a success message or the standardized input
        return input;
    }

    // will switch the color for whomever's turn
    void switchColor()
    {
        if (_colorTurn == "Black")
        {
            _colorTurn = "White";
            return;
        }

        _colorTurn = "Black";
    }

    void placeStone(string &translated)
    {
        _goBoard.placeStone(translated, _colorTurn);
    }

    void printTurn()
    {
        cout << header();
        cout << _goBoard.printBoard();

        string input = askTurn();
        string translated = translateInput(input);
        // if statement loop to see if input is legal
        while (translated == "INCORRECT")
        {
            input = askTurn();
            translated = translateInput(input);
        }

        // Update Turn Order and Turn #
        placeStone(translated);

        switchColor();
        _turn++;
    }

    // DEBUGGING AND TESTING PURPOSES ONLY
    string printBoard()
    {
        return _goBoard.printBoard();
    }
};

#endif // GM_hpp